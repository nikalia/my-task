
import React, { Component } from 'react';
import './App.css';

const App = React.createClass({
  getInitialState() {
    return {
      page:''
    };
  },
  pageA() {
    this.setState({page: 'PageA'});
  },

  pageB() {
    this.setState({page: 'PageB'});
  },

  pageC() {
    this.setState({page: 'PageC'});
  },
  render(){
    const page = this.state.page;

    let button = null;
    if (page === 'PageB') {
      button = <ButtonPageB onClick={this.pageC} />;
    } else if (page === 'PageA') {
      button = <ButtonPageA onClick={this.pageB} />;
    } else if (page === 'PageC'){
      button = <ButtonPageC onClick={this.pageA} />
    } else {
      button = <ButtonPageA onClick={this.pageB} />
    }
    return (
      <div className='App'>
      <div className='App-intro'>
        <Greeting page={page} />
        {button}
        </div>
      </div>
    );
  }
});

function PageA(props) {
  return <h1>This is page A</h1>;
}

function PageB(props) {
  return <h1>This is page B</h1>;
}

function PageC(props) {
  return <h1>This is page C</h1>;
}

function ButtonPageA(props) {
  return (
    <button onClick={props.onClick}>
      Go to page B
    </button>
  );
}

function ButtonPageB(props) {
  return (
    <button onClick={props.onClick}>
      Go to page C
    </button>
  );
}

function ButtonPageC(props) {
  return (
    <button onClick={props.onClick}>
      Go back to page A
    </button>
  );
}

function Greeting(props) {
  const page = props.page;
  if (page === 'PageB') {
    return <PageB />;
  } else if (page === 'PageA') {
    return <PageA />;
  } else if (page === 'PageC'){
    return <PageC />;
  } else {
    return <PageA />;
  }
}

export default App;
